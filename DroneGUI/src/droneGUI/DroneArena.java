package droneGUI;

import java.util.ArrayList;
import java.util.Random;
/**
 * 
 * Class for Arena of Drones(Items)
 */
public class DroneArena {	
	private Random randomGenerator;				// used for random obstacles
	private double xSize, ySize;						// size of arena
	private ArrayList<Item> allItems;			// array list of all balls in arena
	/**
	 * construct an arena of default size
	 */
	DroneArena() {
		this(500, 400);			
	}
	/**
	 * construct arena of size xS by yS
	 * @param xS
	 * @param yS
	 */
	DroneArena(double xS, double yS){
		xSize = xS;
		ySize = yS;
		allItems = new ArrayList<Item>();					
		randomGenerator = new Random();
	}
	/**
	 * return arena size in x direction
	 * @return
	 */
	public double getXSize() {
		return xSize;
	}
	/**
	 * return arena size in y direction
	 * @return
	 */
	public double getYSize() {
		return ySize;
	}
	/**
	 * draw all Items in the arena into canvas mc
	 * @param mc
	 */
	public void drawArena(MyCanvas mc) {
		for (Item I : allItems) I.drawItem(mc);		// draw all Items
	}
	/**
	 * Use the checkItem method for all items in the arraylist 
	 */

	public void checkItems() {
		ArrayList<Item> allItemsClone = (ArrayList<Item>) allItems.clone(); 	//This is used to solve an error caused by deleting items as the program runs. borderpane didnt like items being modified this way while the check is being run. this technique makes a clone list and works using that.
		for (Item I : allItemsClone)I.checkItem(this);	// Calls the checkItemAngle method for all Items, used in DroneBorderPane
	}
	/**
	 * adjust all Items, Redraws the page after
	 */
	public void adjustItems() {
		for (Item I : allItems) I.adjustItem();
	}

	/**
	 * return list of strings defining each Item
	 * @return
	 */
	public ArrayList<String> describeAll() {
		ArrayList<String> ans = new ArrayList<String>();		// set up arraylist
		for (Item I : allItems) ans.add(I.toString());			// add string defining each ball
		return ans;												// return string list
	}
	/** 
	 * Check angle of Drone, if hitting wall, rebound; if hitting another item, change angle
	 * @param x				Item x position
	 * @param y				Item y position
 	 * @param rad			radius
	 * @param ang			current angle
	 * @param notID			identify of Item not to be checked
	 * @return				new angle 
	 */
	public double checkItemAngle(double x, double y, double rad, double ang, int notID) {
		double ans = ang;
		
		if (x < rad || x > xSize - rad) ans = 180 - ans;
			// if ball is about to go through left or right walls, set mirror angle    
		if (y < rad || y > ySize - rad) ans = - ans;
			// if try to go off top or bottom, set mirror angle		
		
		for (Item I : allItems) 
			if (I.getID() != notID && I.hitting(x, y, rad)) ans = 180*Math.atan2(y-I.getY(), x-I.getX())/Math.PI;
				// check all Drones except one with the same ID as our input Item
				// if hitting, return angle between the other Item and this one. (if I want to add height to drones make a method to see if Radius matches)
		
		return ans;	// return the angle
	}
	/**
	 * check if the hunter has hit a drone
	 * @param Drone any drone
	 * @return 	true if hit
	 */
	public boolean checkHit (Item Drone) {		
		boolean ans = false;
    	for (Item I : allItems)
    		if (I instanceof DroneHunter && I.hitting(Drone)) ans = true;
				// try all Items, If there is a hunter hitting a drone return true
		return ans;			//The reason why the hunter doesnt delete itself is because it doesnt do anything with this value. I did experiment with having the hunter multiply when it hits itself but things got out of hand...
	}
	public void removeDrone(Item drone) {
		allItems.remove(drone);
	}
	public void reset() {
		allItems.clear();
	}
	public void addDrone() {
		allItems.add(new Drone(xSize/2, ySize/2, 10, 60, 5));
	}
	public void addConsole() {
		allItems.add(new ConsoleDrone(randomGenerator.nextDouble()*(xSize-10/2), randomGenerator.nextDouble()*(ySize-10/2), 10, 10, 3));  // xSize -10/2 is to account for the radius size of the drone 
	}																																	  // TODO make this automatic 
	public void addHunter() {
		allItems.add(new DroneHunter(randomGenerator.nextDouble()*(xSize-5/2), randomGenerator.nextDouble()*(ySize-5/2), 5, 30, 1));
	}
	public void addObstacle() {
		allItems.add(new Obstacle(randomGenerator.nextDouble()*(xSize-15/2), randomGenerator.nextDouble()*(ySize-15/2), 15));  // places the obstacle randomly within the arena, Some more complex maths will be used to avoid half in obstacles and overlap.
	}																														   // Additionall I plan on making parameters dependant on size of the item so, maybe even make size random and make Items bounce off regardless of size.	
}