package droneGUI;

/**
 * 
 * Abstract class of which, all subsequent Items are based on
 */
public abstract class Item {
	
	protected double x, y, rad;						// position and size of Item
	protected char col;								// used to set colour
	static int ItemCounter = 0;						// used to give each Item a unique identifier
	protected int ItemID;							// unique identifier for each instance
	
	/**
	 * 
	 * Default values
	 */
	Item() {
		this(100, 100, 10);   
	}
	/**
	 * construct a Item of radius ir at ix,iy, not all items have a speed or angle. That is why Drone is also a superclass for items with those variables
	 * @param ix
	 * @param iy
	 * @param ir
	 */
	Item (double ix, double iy, double ir) {
		x = ix;
		y = iy;
		rad = ir;
		ItemID = ItemCounter++;			// set the identifier and increment class static							
	}
	/**
	 * return x position
	 * @return
	 */
	public double getX() { return x; }
	/**
	 * return y position
	 * @return
	 */
	public double getY() { return y; }
	/**
	 * return radius of Item
	 * @return
	 */
	public double getRad() { return rad; }
	/** 
	 * set the Item at position nx,ny
	 * @param nx
	 * @param ny
	 */
	public void setXY(double nx, double ny) {
		x = nx;
		y = ny;
	}
	/**
	 * return the identity of Item
	 * @return ItemID
	 */
	public int getID() {return ItemID; }
	/**
	 * draw a Item into the Canvas mc
	 * @param mc
	 */
	public void drawItem(MyCanvas mc) {
		mc.showCircle(x, y, rad, col);
	}
	/**
	 * I wanted this to be an abstract but eclipse wasn't happy
	 * It sounds bad but I have no idea why this needs to be like this
	 */
	protected String getStrType() {
		return "Item";
	}
	/** 
	 * return string describing Item, see above on why this isn't an abstract
	 */
	public String toString() {
		return getStrType() + " "+ ItemID +" at "+Math.round(x)+", "+Math.round(y);
	}
	/**
	 * abstract method for checking a Item in arena b
	 * @param b
	 */
	protected abstract void checkItem(DroneArena da);
	/**
	 * abstract method for adjusting a Item
	 */
	protected abstract void adjustItem();
	/**
	 * is Item at ox,oy size or hitting this Item     
	 * @param ox
	 * @param oy
	 * @param or
	 * @return true if hitting
	 */
	public boolean hitting(double ox, double oy, double or) {
		return (ox-x)*(ox-x) + (oy-y)*(oy-y) < (or+rad)*(or+rad);
	}		// hitting if dist between Item and ox,oy < ist rad + or
	
	/** is Item hitting the other I used to see if a specific instance of an item is hitting another
	 * 
	 * @param oItem - the other Item
	 * @return true if hitting
	 */
	public boolean hitting (Item oItem) {
		return hitting(oItem.getX(), oItem.getY(), oItem.getRad());
	}
}


