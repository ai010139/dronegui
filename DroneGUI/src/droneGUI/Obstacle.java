package droneGUI;
/**
 * 
 * @author rudim
 * This class an obstacle for other Items to bump into.
 * I have plans to change this into a pillar when adding height into the simulation
 * This means that other Items only collide when their Radius matches,
 * However pillars do not change radius and always cause collisions.	
 */
public class Obstacle extends Item {
	/**
	 * 
	 * @param ix
	 * @param iy
	 * @param ir
	 */
	public Obstacle(double ix, double iy, double ir) {
		super(ix, iy, ir);
	}
	protected String getStrType() {
		return "Obstacle";
	}
	@Override
	protected void checkItem(DroneArena da) {
		// TODO Auto-generated method stub
	}
	@Override
	protected void adjustItem() {
		// TODO Auto-generated method stub

	}
}
