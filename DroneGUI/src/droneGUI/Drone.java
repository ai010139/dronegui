package droneGUI;
/**
 * 
 * Subclass of Item Superclass of moving Items
 */
public class Drone extends Item {
	
	protected double droneAngle;
	protected double droneSpeed;
	/**
	 * 
	 * Initialising values, No need for defaults 
	 * @param ix
	 * @param iy
	 * @param ir
	 * @param iang
	 * @param isp
	 */
	public Drone(double ix, double iy, double ir, double iang, double isp) {
		
		super(ix, iy, ir);
		col = 'r';
		droneAngle = iang;
		droneSpeed = isp;
	}
	/**
	 * Uses the droneArena method checkItemAngle
	 * Other code is for the console drone, ignore!
	 * @param DroneArena
	 */
	@Override
	protected void checkItem(DroneArena da) {	
		droneAngle = da.checkItemAngle(x, y, rad, droneAngle, ItemID);
		if (da.checkHit(this)) da.removeDrone(this);	
	}
	/**
	 * 
	 * adjusts the item, we dont want ALL other Moving items to use this method as that is what differentiates them
	 */
	@Override
	protected void adjustItem() {
		double radAngle = droneAngle*Math.PI/180;	// put angle in radians
		x += droneSpeed * Math.cos(radAngle);		// new X position
		y += droneSpeed * Math.sin(radAngle);		// new Y position
	}
	/**
	 * Generates a string of info, mostly used for testing
	 */
	@Override
	public String toString() {
		return getStrType() + " "+ ItemID +" at "+Math.round(x)+", "+Math.round(y) + " angle" + Math.round(droneAngle);
	}
	/**
	 * Stringtype, explains itself
	 * huh.
	 */
	@Override
	protected String getStrType() {
		return "Classic Drone";
	}
}
