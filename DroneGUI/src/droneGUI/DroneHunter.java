package droneGUI;
/**
 * 
 * @author rudim
 *This class will "kill" any drone it comes into contact with (remove that instance from arraylist)
 *Right now it doesn't move, that is to test the collision with drones
 *Ideally the drone hunter will chase other drones, either when within a certain radius of the hunter or just all the time but the hunter is slower than the drones.
 *This might be out of the scope of the assignment but I would like to make it after perhaps
 */
public class DroneHunter extends Drone {
	/**
	 * constructor, using super() is nice
	 * @param ix
	 * @param iy
	 * @param ir
	 * @param iang
	 * @param isp
	 */
	public DroneHunter(double ix, double iy, double ir, double iang, double isp) {
		super(ix, iy, ir, iang, isp);
		col = 'b';
	}
	/**
	 * Checks for collisions with items and wall
	 */
	protected void checkItem(DroneArena da) {
		droneAngle = da.checkItemAngle(x, y, rad, droneAngle, ItemID);
	}
	/**
	 * The displayed name for this item
	 */
	protected String getStrType() {
		return "Drone Hunter";
	}
}
