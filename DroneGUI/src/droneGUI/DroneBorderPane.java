package droneGUI;

import java.util.ArrayList;

import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * Creates the GUI
 * has a droneArena and a canvas, controls the arena.
 */
public class DroneBorderPane extends Application {
	private MyCanvas mc;
	private AnimationTimer timer;								// timer used for animation
	private VBox rtPane;										// vertical box for putting info
	private DroneArena da;
	/**
	 * Adds an about button 	
	 */
	private void showAbout() {
	    Alert alert = new Alert(AlertType.INFORMATION);				// initiallises a smaller popup box to be used in setMenu method, once the about menu is clicked this box comes up
	    alert.setTitle("About");									// title of the box 
	    alert.setHeaderText(null);									// set header text	
	    alert.setContentText("Rudi's Drone Simulation: If you want to reset the simulation click file then reset. Add Items using the buttons at the bottom of the window, experiment to see how each works. To begin the simulation hit start. Have fun :) ");		
	    alert.showAndWait();										// show box and wait for user to close
	}
	/**
	 * set up the menu of commands for the GUI
	 * @return the menu bar
	 */
	private MenuBar setMenu() {
		MenuBar menuBar = new MenuBar();						// create main menu
	
		Menu mFile = new Menu("File");							// add File main menu
		MenuItem mExit = new MenuItem("Exit");					// create Exit sub menu
		MenuItem mReset = new MenuItem("Reset");            	// create Reset sub menu
		mExit.setOnAction(new EventHandler<ActionEvent>() {
		    public void handle(ActionEvent t) {					// action on exit is
	        	timer.stop();									// stop timer
		        System.exit(0);									// exit program
		    }   
		});
		mReset.setOnAction(new EventHandler<ActionEvent>() {	
			 public void handle(ActionEvent t) {		
				 da.reset();									// Clear the arraylist allItems
			 }
		});
		
		mFile.getItems().addAll(mExit);							// add exit to File menu
		mFile.getItems().addAll(mReset);						// add Reset to File menu
		
		Menu mHelp = new Menu("Help");							// create Help menu
		MenuItem mAbout = new MenuItem("About");				// add About sub menu
		mAbout.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
            	showAbout();									// and its action to print about
            }	
		});
		mHelp.getItems().addAll(mAbout);						// add About to Help main item
		
		menuBar.getMenus().addAll(mFile, mHelp);				// set main menu with File, Help
		return menuBar;											// return the menu
	}
	/**
	 * set up the horizontal box for the bottom with relevant buttons
	 * @return Hbox
	 */
	private HBox setButtons() {
	    Button btnStart = new Button("Start");					// create button for starting
	    btnStart.setOnAction(new EventHandler<ActionEvent>() {	// now define event when it is pressed
	        @Override
	        public void handle(ActionEvent event) {
	        	timer.start();									// its action is to start the timer
	       }
	    });

	    Button btnStop = new Button("Pause");					// now button for stop
	    btnStop.setOnAction(new EventHandler<ActionEvent>() {
	        @Override
	        public void handle(ActionEvent event) {
	           	timer.stop();									// and its action to stop the timer
	       }
	    });

	    Button btnAddDr = new Button("Add Drone");				//Adds a button to add drone
	    btnAddDr.setOnAction(new EventHandler<ActionEvent>() {
	        @Override
	        public void handle(ActionEvent event) {
	           	da.addDrone();								
	           	drawWorld();									//redraw the world after
	       }
	        
	    });
	    Button btnAddOb = new Button("Add Obstacle");			//add Obstacle	
	    btnAddOb.setOnAction(new EventHandler<ActionEvent>() {
	        @Override
	        public void handle(ActionEvent event) {
	           	da.addObstacle();							
	           	drawWorld();	
	        }
	    });
	    Button btnAddHtr = new Button("Add Hunter");			//add Hunter	
	    btnAddHtr.setOnAction(new EventHandler<ActionEvent>() {
	        @Override
	        public void handle(ActionEvent event) {
	           	da.addHunter();								
	           	drawWorld();	
	        }
	    });
	    Button btnAddCon = new Button("Add ConsoleDrone");		//add ConsoleDrone		
	    btnAddCon.setOnAction(new EventHandler<ActionEvent>() {
	        @Override
	        public void handle(ActionEvent event) {
	           	da.addConsole();								
	           	drawWorld();	
	        }
	    });
	    return new HBox(new Label("Run: "), btnStart, btnStop, new Label("Add: "), btnAddDr, btnAddOb, btnAddHtr, btnAddCon); // now add these buttons + labels to a HBox seperated into the animation controls and add item buttons
	}
	/** 
	 * clear canvas then drawArena
	 *	
	 */
	public void drawWorld () {
	 	mc.clearCanvas();						
	 	da.drawArena(mc);
	}
	/**
	 * Display all Item info on the right pane
	 */
	public void drawStatus() {
		rtPane.getChildren().clear();					// clear rtpane
		ArrayList<String> allIs = da.describeAll();
		for (String s : allIs) {
			Label l = new Label(s); 		// turn description into a label
			rtPane.getChildren().add(l);	// add label	
		}	
	}
	/** 
	 * 	Main method for setting stage. Creates the canvas and arena, sets up the GUI
	 */
	@Override
	public void start(Stage primaryStage) throws Exception {
		// TODO Auto-generated method stub
		primaryStage.setTitle("Rudi's overly ambitious drone simulation that he had to cut down on for time"); //title of the window
	    BorderPane bp = new BorderPane();
	    bp.setPadding(new Insets(10, 20, 10, 20));						//note to self dont try and change this.... didn't go so well last time eh.

	    bp.setTop(setMenu());											// put menu at the top

	    Group root = new Group();										// create group with canvas
	    Canvas canvas = new Canvas( 400, 500 );
	    root.getChildren().add( canvas );
	    bp.setLeft(root);												// load canvas to left area
	
	    mc = new MyCanvas(canvas.getGraphicsContext2D(), 400, 500);		

	    da = new DroneArena(400, 500);								    // set up arena
	    drawWorld();
	    
	    timer = new AnimationTimer() {									// set up timer
	        public void handle(long currentNanoTime) {					// and its action when on
	        		da.checkItems();									// check the angle of all balls
		            da.adjustItems();								    // move all balls
		            drawWorld();										// redraw the world
		            drawStatus();										// indicate where balls are
	        }
	    };

	    rtPane = new VBox();											// set vBox on right to list items
		rtPane.setAlignment(Pos.TOP_LEFT);								// set alignment
		rtPane.setPadding(new Insets(5, 75, 75, 5));					// padding
 		bp.setRight(rtPane);											// add rtPane to borderpane right
		  
	    bp.setBottom(setButtons());										// set bottom pane with buttons

	    Scene scene = new Scene(bp, 700, 600);							// set overall scene
        bp.prefHeightProperty().bind(scene.heightProperty());
        bp.prefWidthProperty().bind(scene.widthProperty());

        primaryStage.setScene(scene);
        primaryStage.show();
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
	    Application.launch(args);			// launch the GUI

	}

}